# Polish translations for live-manual
# Copyright (C) 2014 AreYouLoco? <areyouloco@paranoici.org>
# This file is distributed under the same license as the live-manual package.
#
msgid ""
msgstr ""
"Project-Id-Version: live-manual 4.0~a4-1\n"
"POT-Creation-Date: 2014-08-31 12:33+0300\n"
"PO-Revision-Date: 2014-11-08 19:58+0100\n"
"Last-Translator: AreYouLoco? <areyouloco@paranoici.org>\n"
"Language-Team: none\n"
"Language: pl_PL\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 1.5.4\n"

#. type: Content of: <html><head><title>
#: en/index.html.in:4
msgid "Live Systems Project"
msgstr "Projekt Systemów Live"

#. type: Content of: <html><body><h2>
#: en/index.html.in:9
msgid "Live Systems Manual"
msgstr "Podręcznik Systemów Live"

#. type: Content of: <html><body><p>
#: en/index.html.in:12
msgid ""
"<i>live-manual</i> is available in different file formats and it is "
"translated into several languages. Keep in mind that some translations may "
"be incomplete or may not be up to date."
msgstr ""
"<i>live-manual</i> jest dostępny w różnych formatach plików i przetłumaczony "
"na różne języki. Miej na uwadze, że niektóre tłumaczenia mogą być "
"niekompletne i/lub nieaktualne oraz zawierać błędy."

#. type: Content of: <html><body><p>
#: en/index.html.in:17
msgid ""
"Please report errors, omissions, patches and suggestions to our mailing list "
"at <a href=\"mailto:debian-live@lists.debian.org\">debian-live@lists.debian."
"org</a> and read about <a href=\"html/live-manual.en.html#how-to-contribute"
"\">how to contribute</a> to the manual."
msgstr ""
"Proszę zgłaszaj błędy, poprawki, patche i sugestie na naszą listę mailingową "
"na <a href=\"mailto:debian-live@lists.debian.org\">debian-live@lists.debian."
"org</a> i dowiedź się <a href=\"html/live-manual.pl.html#how-to-contribute"
"\">jak przyczynić się </a> do rozwoju podręcznika."

#. type: Content of: <html><body><h3>
#: en/index.html.in:20
msgid "Available Formats"
msgstr "Dostępne Formaty"

#. type: Content of: <html><body><ul><li>
#: en/index.html.in:23
msgid "<a href=\"epub/live-manual.en.epub\">EPUB</a>"
msgstr "<a href=\"epub/live-manual.pl.epub\">EPUB</a>"

#. type: Content of: <html><body><ul><li>
#: en/index.html.in:24
msgid ""
"HTML: <a href=\"html/live-manual/toc.en.html\">multi page</a>, <a href="
"\"html/live-manual.en.html\">single page</a>"
msgstr ""
"HTML: <a href=\"html/live-manual/toc.pl.html\">na wielu podstronach</a>, <a "
"href=\"html/live-manual.pl.html\">wszystko na jednej stronie</a>"

#. type: Content of: <html><body><ul><li>
#: en/index.html.in:25
msgid "<a href=\"odt/live-manual.en.odt\">ODF</a>"
msgstr "<a href=\"odt/live-manual.pl.odt\">ODF</a>"

#. type: Content of: <html><body><ul><li>
#: en/index.html.in:26
msgid ""
"PDF: <a href=\"pdf/live-manual.portrait.en.a4.pdf\">A4 portrait</a>, <a href="
"\"pdf/live-manual.landscape.en.a4.pdf\">A4 landscape</a>, <a href=\"pdf/live-"
"manual.portrait.en.letter.pdf\">letter portrait</a>, <a href=\"pdf/live-"
"manual.landscape.en.letter.pdf\">letter landscape</a>"
msgstr ""
"PDF: <a href=\"pdf/live-manual.portrait.pl.a4.pdf\">A4 pionowy</a>, <a href="
"\"pdf/live-manual.landscape.pl.a4.pdf\">A4 poziomy</a>, <a href=\"pdf/live-"
"manual.portrait.pl.letter.pdf\">listowy pionowy</a>, <a href=\"pdf/live-"
"manual.landscape.pl.letter.pdf\">listowy poziomy</a>"

#. type: Content of: <html><body><ul><li>
#: en/index.html.in:27
msgid "<a href=\"txt/live-manual.en.txt\">Plain text</a>"
msgstr "<a href=\"txt/live-manual.pl.txt\">Zwykły Tekst</a>"

#. type: Content of: <html><body><p>
#: en/index.html.in:31
msgid "Last changed: @DATE_CHANGE@"
msgstr "Data ostatniej zmiany: @DATE_CHANGE@"

#. type: Content of: <html><body><p>
#: en/index.html.in:32
msgid "Last built: @DATE_BUILD@"
msgstr "Data ostatniego build'u: @DATE_BUILD@"

#. type: Content of: <html><body><h3>
#: en/index.html.in:35
msgid "<a href=\"search.cgi\">Search</a>"
msgstr "<a href=\"search.cgi\">Szukaj</a>"

#. type: Content of: <html><body><h3>
#: en/index.html.in:37
msgid "Source"
msgstr "Źródło"

#. type: Content of: <html><body><p>
#: en/index.html.in:40
msgid ""
"The sources for this manual are available in a <a href=\"http://git-scm.com/"
"\">Git</a> repository at debian-live.alioth.debian.org."
msgstr ""
"Źródło do tego podręcznika jest dostępne w repozytorium <a href=\"http://git-"
"scm.com/\">Git</a> na stronie debian-live.alioth.debian.org."

#. type: Content of: <html><body><ul><li>
#: en/index.html.in:44
msgid ""
"Browse: <a href=\"http://http://anonscm.debian.org/cgit/debian-live/live-manual.git"
"\"><small><tt>http://http://anonscm.debian.org/cgit/debian-live/live-manual.git</tt></"
"small></a>"
msgstr ""
"Przeglądaj: <a href=\"http://http://anonscm.debian.org/cgit/debian-live/live-manual.git"
"\"><small><tt>http://http://anonscm.debian.org/cgit/debian-live/live-manual.git</tt></"
"small></a>"

#. type: Content of: <html><body><ul><li>
#: en/index.html.in:45
msgid ""
"Git: <a href=\"git://http://anonscm.debian.org/git/debian-live/live-manual.git\"><small><tt>git://"
"http://anonscm.debian.org/git/debian-live/live-manual.git</tt></small></a>"
msgstr ""
"Git: <a href=\"git://http://anonscm.debian.org/git/debian-live/live-manual.git\"><small><tt>git://"
"http://anonscm.debian.org/git/debian-live/live-manual.git</tt></small></a>"

#. type: Content of: <html><body><p>
#: en/index.html.in:49
msgid ""
"<a href=\"http://debian-live.alioth.debian.org/\">Live Systems Project</a> &lt;<a href="
"\"mailto:debian-live@lists.debian.org\">debian-live@lists.debian.org</a>&gt; "
"- <a href=\"http://debian-live.alioth.debian.org/project/legal/\">Legal Information</a>"
msgstr ""
"<a href=\"http://debian-live.alioth.debian.org/\">Projekt Live Systems</a> &lt;<a href="
"\"mailto:debian-live@lists.debian.org\">debian-live@lists.debian.org</a>&gt; "
"- <a href=\"http://debian-live.alioth.debian.org/project/legal/\">Informacje prawne</a>"
